package com.example.calculator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {
    private TextView value;
    private boolean isDot;
    private boolean afterDot;
    private boolean isEvaluated;
    private Double firstNumber;
    private String operation;
    private TextView message;
    private TextView firstNumberView;
    private TextView operationView;
    private boolean isMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.value = findViewById(R.id.value);
        this.isDot = false;
        this.isEvaluated = false;
        this.firstNumber = null;
        this.operation = null;
        this.message = findViewById(R.id.message);
        this.isMessage = true;
        this.firstNumberView = findViewById(R.id.firstNumber);
        this.operationView = findViewById(R.id.operation);
        System.out.println("CREATED");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        String temp = "";
        String tempFn = "";
        if(this.operation != null){
            temp = this.operationView.getText().toString();
            tempFn = this.firstNumberView.getText().toString();
        }
        outState.putString("operation", temp);
        outState.putString("message", this.message.getText().toString());
        outState.putString("firstNumber", tempFn);
        outState.putString("current", this.value.getText().toString());
        outState.putBoolean("isEval", this.isEvaluated);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String op = savedInstanceState.getString("operation", "");
        String mes = savedInstanceState.getString("message", "");
        String fN = savedInstanceState.getString("firstNumber", "");
        String curr = savedInstanceState.getString("current", "0.0");
        Boolean isE = savedInstanceState.getBoolean("isEval", false);
        if(op != ""){
            this.operation = op;
            this.operationView.setText(op);
        }
        if(mes != ""){
            this.message.setText(mes);
            this.isMessage = true;
        }
        if(fN != ""){
            this.firstNumber = Double.parseDouble(fN);
            this.firstNumberView.setText(fN);
        }
        this.value.setText(curr);
    }

    public void number(View view){
        if(this.isMessage){
            clearMessageView();
        }
        if(this.value.getText().toString().equals("0") || this.isEvaluated) {
            this.isEvaluated = false;
            this.value.setText(view.getTag().toString());
        }
        else{
            if(this.isDot) {
                if(this.afterDot){
                    Integer number = Integer.parseInt(view.getTag().toString());
                    String prevText = this.value.getText().toString();
                    String numberStr = "." + number;
                    this.value.setText(prevText.replace(".0", numberStr));
                    this.afterDot = false;
                }
                else{
                    Integer number = Integer.parseInt(view.getTag().toString());
                    String prevText = this.value.getText().toString();
                    this.value.setText(prevText + number);
                }
            }
            else{
                Integer number = Integer.parseInt(view.getTag().toString());
                String prevText = this.value.getText().toString();
                this.value.setText(prevText + number);
            }
        }
    }

    public void dot(View view){
        if(this.isMessage){
            clearMessageView();
        }
        if(!this.isDot){
            this.value.setText(this.value.getText().toString() + ".0");
            this.isDot = true;
            this.afterDot = true;
        }
    }

    public void negative(View view){
        if(this.isMessage){
            clearMessageView();
        }
        if(!this.value.getText().toString().equals("0")){
            Double newValue = -Double.parseDouble(this.value.getText().toString());
            this.value.setText(newValue.toString());
        }
    }

    public void reset(View view){
        if(this.isMessage){
            clearMessageView();
        }
        this.value.setText("0");
        this.isDot = false;
        this.isEvaluated = false;
        this.firstNumber = null;
        this.operation = null;
        this.firstNumberView.setText("");
        this.operationView.setText("");
        this.message.setText("Zresetowano pamięć!");
    }

    public void summary(View view){
        clearOperationView();
        clearFirstNumberView();
        if(this.isMessage){
            clearMessageView();
        }
        if(this.operation != null && !this.isEvaluated){
            Double secondNumber = Double.parseDouble(this.value.getText().toString());
            this.isEvaluated = true;
            Double result = this.firstNumber;
            switch (this.operation){
                case "+":
                    result += secondNumber;
                    this.message.setText("Wykonano operację dodawania!");
                    break;
                case "-":
                    result -= secondNumber;
                    this.message.setText("Wykonano operację odejmowania!");
                    break;
                case "*":
                    result *= secondNumber;
                    this.message.setText("Wykonano operację mnożenia!");
                    break;
                default:
                    result /= secondNumber;
                    this.message.setText("Wykonano operację dzielenia!");
            }
            String resultStr = result.toString();

            resultStr = checkDotIsNeed(resultStr);
            this.value.setText(resultStr);
        }
    }

    public void addOperator(View view) {
        this.firstNumber = Double.parseDouble(this.value.getText().toString());
        this.firstNumberView.setText(checkDotIsNeed(this.firstNumber.toString()));
        this.operationView.setText(view.getTag().toString());
        this.value.setText("0");
        this.operation = view.getTag().toString();
    }

    private String checkDotIsNeed(String resultStr){
        int l = resultStr.length() - 1;
        if(     resultStr.charAt(l - 1) == '.'
                && resultStr.charAt(l) == '0'   ){
            this.isDot = false;
            resultStr = resultStr.replace(".0","");
        }

        return resultStr;
    }

    public void percentage(View view){
        if(!this.isEvaluated) {
            clearOperationView();
            clearFirstNumberView();
            if (this.isMessage) {
                clearMessageView();
            }
            this.firstNumber = Double.parseDouble(this.value.getText().toString());
            Double result = this.firstNumber / 100.0;
            String resultStr = checkDotIsNeed(result.toString());
            this.value.setText(resultStr);
            this.message.setText("Wykonano operację %!");
            this.operation = "%";
        }
    }

    public void log(View view){
        if(!this.isEvaluated) {
            Double log = Math.log10(Double.parseDouble(this.value.getText().toString()));
            this.value.setText(log.toString());
            this.message.setText("Wykonano operację log10!");
        }
    }

    public void factorial(View view){
        if(!this.isEvaluated) {
            Integer n = Integer.parseInt(this.value.getText().toString());
            Integer fx = f(n);
            this.value.setText(fx.toString());
            this.message.setText("Wykonano operację silni!");
        }
    }

    private int f(int i){
        if (i < 1) {
            return 1;
        }
        else {
            return i * f(i - 1);
        }
    }

    public void sqrt(View view){
        if(!this.isEvaluated) {
            Double sqrtx = Math.sqrt(Double.parseDouble(this.value.getText().toString()));
            this.value.setText(sqrtx.toString());
            this.message.setText("Wykonano operację pierwiastkowania!");
        }
    }

    public void cube(View view){
        if(!this.isEvaluated) {
            Double cubex = Math.pow(Double.parseDouble(this.value.getText().toString()), 3);
            this.value.setText(cubex.toString());
            this.message.setText("Wykonano operację potęgowania ^3!");
        }
    }

    public void square(View view){
        if(!this.isEvaluated) {
            Double squarex = Math.pow(Double.parseDouble(this.value.getText().toString()), 3);
            this.value.setText(squarex.toString());
            this.message.setText("Wykonano operację potęgowania ^2!");
        }
    }

    private void clearMessageView(){
        this.message.setText("");
    }

    private void clearOperationView(){
        this.operationView.setText("");
    }

    private void clearFirstNumberView(){
        this.firstNumberView.setText("");
    }
}
